<footer class="footer mt-auto" role="contentinfo">
	<div class="inner container">
        <div class="row flex-column-reverse flex-md-row">
            <div class="brand col-md text-center text-md-left">
                <img class="brand-logo align-middle " src="$ThemeDir/images/scu-logo web-xxs (85x100).png" alt="uster rhinos logo">
                <p class="brand-address d-inline-block align-middle ">
                    Uster Rhinos<br>
                    Seestrasse 58<br>
                    8610 Uster
                </p>
            </div>
            <%-- <ul class="nav col-md flex-column">
                <% loop $Menu(1) %>
                    <li class="nav-item">
                        <a class="nav-link no-link-style $LinkingMode" href="$Link" title="$Title.XML">
                            <i class="bi bi-chevron-right"></i> $MenuTitle.XML
                        </a>
                    </li>
                <% end_loop %>
            </ul> --%>
        </div>
        <small>Theme and Website by Serafino Fornito</small>
	</div>
</footer>
