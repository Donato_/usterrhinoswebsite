<!DOCTYPE html>
<html lang="$ContentLocale">
    <head>
        <% base_tag %>
        <title><% if $MetaTitle %>$MetaTitle<% else %>$Title<% end_if %> &raquo; $SiteConfig.Title</title>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        $MetaTags(false)

        <%-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> --%>

        <!-- Bootstrap CSS -->
        <% require css('//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css') %>
        <% require css('//cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css') %>

        <%-- <% require themedCSS('reset') %> --%>
        <%-- <% require themedCSS('typography') %> --%>
        <%-- <% require themedCSS('layout') %> --%>
        <% require themedCSS('mylayout') %>

        <!-- define tab icon -->
        <link rel="shortcut icon" href="$ThemeDir/images/scu-logo web-xxs (85x100).png" />
    </head>
    <body class="$ClassName.ShortName<% if not $Menu(2) %> no-sidebar<% end_if %> d-flex flex-column" <% if $i18nScriptDirection %>dir="$i18nScriptDirection"<% end_if %>>
        <% include Header %>

        <div class="main" role="main">
            <div class="inner container">
                $Layout <%-- includes the correct ".ss"-file under "templates/Layout" --%>
            </div>
        </div>

        <% include Footer %>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <% require javascript('//code.jquery.com/jquery-3.3.1.min.js') %>
        <% require javascript('//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js') %>
        <% require javascript('//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js') %>
        <%-- <% require themedJavascript('script') %> --%>

        <%-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> --%>
    </body>
</html>
