<?php

use SilverStripe\Admin\ModelAdmin;

class Categories extends ModelAdmin
{
private static $menu_title = "Categories";
private static $url_segment = "categories";

    private static $managed_models = [
        VideoCategory::class
    ];
}
