<?php

use SilverStripe\Forms\DateField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class MemberExtension extends DataExtension
{
    private static $db = [
        "BirthdayDate" => "Date",
        "City" => "Varchar",
        "Country" => "Varchar",
        "ZipCode" => "Varchar",
        "Street" => "Varchar",
        "StreetNumber" => "Varchar",
        "Latitude" => "Varchar",
        "Longitude" => "Varchar",
    ];

    private static $has_one = [];

    private static $has_many = [
        "MaterialOrders" => MaterialOrder::class,
        "Invoices" => Invoice::class,
    ];

    private static $many_many = [];

    private static $belongs_many_many = [];

    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldToTab("Root.Main", DateField::create("BirthdayDate"), "Email");
        $fields->addFieldsToTab("Root.Address", [
            TextField::create("Street"),
            TextField::create("StreetNumber"),
            TextField::create("ZipCode"),
            TextField::create("City"),
            TextField::create("Country"),
            TextField::create("Latitude"),
            TextField::create("Longitude"),
        ]);
    }
}
