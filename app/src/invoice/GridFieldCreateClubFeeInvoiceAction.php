<?php

use SilverStripe\Security\Member;
use SilverStripe\Control\Controller;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridField_FormAction;
use SilverStripe\Forms\GridField\GridField_ActionProvider;
use SilverStripe\Forms\GridField\GridField_ColumnProvider;

// https://docs.silverstripe.org/en/4/developer_guides/forms/how_tos/create_a_gridfield_actionprovider/

class GridFieldCreateClubFeeInvoiceAction implements GridField_ColumnProvider, GridField_ActionProvider
{
    const COLUMN_TITEL = "Actions";

    public function augmentColumns($gridField, &$columns)
    {
        if (!in_array(self::COLUMN_TITEL, $columns)) {
            $columns[] = self::COLUMN_TITEL;
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName)
    {
        return ["class" => "grid-field__col-compact"];
    }

    public function getColumnMetadata($gridField, $columnName)
    {
        if ($columnName === self::COLUMN_TITEL) {
            return ["title" => ""];
        }
    }

    public function getColumnsHandled($gridField)
    {
        return [self::COLUMN_TITEL];
    }

    public function getColumnContent($gridField, $record, $columnName)
    {
        if (!$record->canEdit()) {
            return;
        }

        $field = GridField_FormAction::create(
            $gridField,
            "CreateClubfeeInvoice" . $record->ID,
            "Create Clubfee Invoice",
            "docreateclubfeeinvoiceaction",
            ["RecordID" => $record->ID]
        )->addExtraClass("btn btn-primary");

        return $field->Field();
    }

    public function getActions($gridField)
    {
        return ["docreateclubfeeinvoiceaction"];
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data)
    {
        if ($actionName !== "docreateclubfeeinvoiceaction") {
            return;
        }
        // perform your action here

        $member = Member::get_by_id($arguments["RecordID"]);
        if (!is_null($member)) {
            //___creating new invoice___//
            $birthdate = $member->dbObject("BirthdayDate");
            $currentDate = DBField::create_field("Date", date("Y-m-d"));
            $age = $currentDate->Year() - $birthdate->Year();

            if ($age < 18) {
                $filterString = "Jugendlich";
            } else {
                $filterString = "Erwachsen";
            }

            $clubfeeProduct = Product::get()->filter([
                "Category.Title" => "Mitgliederbeitrag",
                "Name:PartialMatch" => $filterString,
            ])->sort("Price", "DESC")->first();

            Invoice::createAndSendInvoice("Mitgliederbeitrag " . $currentDate->Year(), $member, [$clubfeeProduct]);
        } else {
            // output an error message to the user
            Controller::curr()->getResponse()->setStatusCode(
                404,
                "No member with the ID " . $arguments["RecordID"] . " found."
            );
        }
    }
}
