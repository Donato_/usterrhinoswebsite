<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Member;
use SilverStripe\Control\Director;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Email\Email;
use SilverStripe\Security\Permission;
use SilverStripe\Security\PermissionProvider;

class Invoice extends DataObject implements PermissionProvider
{
    private static $db = [
        "Title" => "Varchar",
        "LastReminder" => "Date",
        "Deadline" => "Date",
        "Total" => "Currency",
        "IsPaid" => "Boolean",
    ];

    private static $has_one = [
        "Member" => Member::class,
    ];

    private static $many_many = [
        "Products" => Product::class,
    ];

    private static $many_many_extraFields = [
        "Products" => [
            "PurchasedPrice" => "Currency",
            "Quantity" => "Int",
            "PriceTotal" => "Currency",
        ],
    ];

    public function populateDefaults()
    {
        $this->Deadline = date("Y-m-d", strtotime("+ 60 days"));
        parent::populateDefaults();
    }

    private static $summary_fields = [
        "Created.Nice" => "Created",
        "Member.Name" => "For Member",
        "Title" => "Title",
        "Total" => "Total Price",
        "IsPaid" => "Is paid?",
    ];

    const CMS_PERMISSION_CODE = "CMS_ACCESS_" . __CLASS__;

    public function providePermissions()
    {
        $implodedClassName = strtolower(implode(" ", preg_split('/(?=[A-Z])/', __CLASS__)));
        $permissionsArr = [];
        foreach (["view", "edit", "delete", "create"] as $value) {
            $permissionsArr[strtoupper(self::CMS_PERMISSION_CODE . "_" . $value)] = [
                "name" => "Can $value $implodedClassName",
                "category" => "CMS administrate '" . __CLASS__ . "'",
                "help" => "Gives permissions to $value $implodedClassName",
            ];
        }
        return $permissionsArr;
    }

    public function canView($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_VIEW"), "any", $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_EDIT"), "any", $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_DELETE"), "any", $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_CREATE"), "any", $member);
    }

    public function getCMSfields()
    {
        $fields = Parent::getCMSFields();

        $fields->removeFieldsFromTab("Root.Main", [
            "Total",
        ]);

        return $fields;
    }

    public function onBeforeWrite()
    {
        $this->Total = array_sum($this->Products()->column("PriceTotal"));

        parent::onBeforeWrite();
    }

    public static function createAndSendInvoice(string $title, Member $member, $products)
    {
        $invoice = Invoice::create();
        $invoice->Title = $title;
        $invoice->Member = $member;
        foreach ($products as $product) {
            $quantity = $product->Quantity > 0 ? $product->Quantity : 1;
            $invoice->Products()->add($product, [
                "PurchasedPrice" => $product->Price,
                "Quantity" => $quantity,
                "PriceTotal" => $quantity * $product->Price,
            ]);
        }
        $invoice->write();

        //___generating PDF___//
        if ($invoice->ID) {
            $pdf = new TCPDF();
            // remove default header/footer
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
            $pdf->AddPage();
            $html = $invoice->renderWith("Invoice");
            $pdf->writeHTML($html, true, false, true, false, "");
            $base64string = $pdf->Output("$title ($invoice->ID).pdf", "S");

            if (!Director::isLive()) {
                $dir_name = __DIR__ . "/generated-pdfs";
                if (!is_dir($dir_name)) {
                    mkdir($dir_name);
                }

                $pdf->Output("$dir_name/$title ($invoice->ID).pdf", "F");
            }

            //___creating and sending email___//
            $from = "info@uster-rhinos.ch";
            $to = $member->Email;
            $testSubject = strpos($_SERVER['SERVER_NAME'], "test") === false ? "" : "(TEST)";
            $subject = $testSubject . $invoice->Title;
            $body = $invoice->renderWith("ClubfeeInvoiceEmail");
            $email = Email::create($from, $to, $subject, $body)->setHTMLTemplate("EmailBody");

            $email->addAttachmentFromData($base64string, $invoice->Title . ".pdf", "base64");

            if ($email->send()) {
                // output a success message to the user
                Controller::curr()->getResponse()->setStatusCode(
                    200,
                    "Sent new club fee invoice for $member->Name to $member->Email."
                );
            } else {
                // output an error message to the user
                Controller::curr()->getResponse()->setStatusCode(
                    400,
                    "There may have been 1 or more failures with sending the email."
                );
            }
        } else {
            // output an error message to the user
            Controller::curr()->getResponse()->setStatusCode(
                500,
                "There has been a problem creating the invoice dataobject."
            );
        }
    }
}
