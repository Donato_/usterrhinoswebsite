<?php

use SilverStripe\Security\Member;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Forms\GridField\GridFieldConfig;

class InvoicesAdmin extends ModelAdmin
{
    private static $menu_title = "Invoices";
    private static $url_segment = "invoices";

    private static $managed_models = [
        Invoice::class,
        Member::class,
    ];

    protected function getGridFieldConfig(): GridFieldConfig
    {
        $config = parent::getGridFieldConfig();

        // modify the list view.
        if ($this->modelClass === Member::class) {
            $config->addComponent(new GridFieldCreateClubFeeInvoiceAction());
        }

        return $config;
    }
}
