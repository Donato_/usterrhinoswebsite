<?php

use SilverStripe\Security\Security;
use SilverStripe\Control\Controller;

class MaterialOrderPageController extends PageController
{
    private static $allowed_actions = [
        "PlaceOrder",
    ];

    public function PlaceOrder()
    {
        $currentUser = Security::getCurrentUser();

        if ($currentUser) {
            $materialOrder = MaterialOrder::create();
            $materialOrder->Member = $currentUser;
            foreach ($_POST["entries"] as $productId => $orderedQuantity) {
                if ($orderedQuantity > 0) {
                    $product = Product::get_by_id($productId);
                    $materialOrder->Products()->add($product, [
                        "PurchasedPrice" => $product->Price,
                        "Quantity" => $orderedQuantity,
                        "PriceTotal" => $orderedQuantity * $product->Price,
                    ]);
                }
            }
            $materialOrder->write();

            if ($materialOrder->ID) {
                Invoice::createAndSendInvoice("Materialbestellung", $currentUser, $materialOrder->Products());
            } else {
                // output an error message to the user
                Controller::curr()->getResponse()->setStatusCode(
                    404,
                    "No user currently logged in. Please login to place an order."
                );
            }

        } else {
            // output an error message to the user
            Controller::curr()->getResponse()->setStatusCode(
                404,
                "No user currently logged in. Please login to place an order."
            );
        }

        $this->redirectBack();
    }
}
