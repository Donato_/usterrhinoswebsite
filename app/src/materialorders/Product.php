<?php

use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\CurrencyField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Security\Permission;
use SilverStripe\Security\PermissionProvider;
use SilverStripe\AssetAdmin\Forms\UploadField;

class Product extends DataObject implements PermissionProvider
{
    private static $db = [
        "Name" => "Varchar",
        "Price" => "Currency",
        "IsActive" => "Boolean",
    ];

    private static $has_one = [
        "Category" => Category::class,
        "Image" => Image::class,
    ];

    private static $belongs_many_many = [
        "MaterialOrders" => MaterialOrder::class,
        "Invoices" => Invoice::class,
    ];

    private static $summary_fields = array(
        "IsActive" => "Is active?",
        "Name" => "Name",
        "Category.Title" => "Category",
        "Price" => "Price",
    );

    const CMS_PERMISSION_CODE = "CMS_ACCESS_" . __CLASS__;

    public function providePermissions()
    {
        $implodedClassName = strtolower(implode(" ", preg_split('/(?=[A-Z])/', __CLASS__)));
        $permissionsArr = [];
        foreach (["view", "edit", "delete", "create"] as $value) {
            $permissionsArr[strtoupper(self::CMS_PERMISSION_CODE . "_" . $value)] = [
                "name" => "Can $value $implodedClassName",
                "category" => "CMS administrate '" . __CLASS__ . "'",
                "help" => "Gives permissions to $value $implodedClassName",
            ];
        }
        return $permissionsArr;
    }

    public function canView($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_VIEW"), "any", $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_EDIT"), "any", $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_DELETE"), "any", $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_CREATE"), "any", $member);
    }

    public function getCMSfields()
    {
        $fields = FieldList::create(TabSet::create("Root"));
        $fields->addFieldsToTab("Root.Main", [
            CheckboxField::create("IsActive", "Is active?"),
            TextField::create("Name"),
            CurrencyField::create("Price", "Price (per piece)"),
            DropdownField::create("CategoryID", "Category")
                ->setSource(Category::get()->map("ID", "Title")),
            $upload = UploadField::create(
                "Image",
                "Product image"
            ),
        ]);

        $upload->getValidator()->setAllowedExtensions(array(
            "png", "jpeg", "jpg", "gif",
        ));
        $upload->setFolderName("product-images");

        return $fields;
    }

    public static function getAllProducts($filterValue = null)
    {
        $products = Product::get();
        if (!is_null($filterValue)) {
            $products = $products->filter(["IsActive" => $filterValue]);
        }

        return $products->sort("Name");
    }
}
