<?php

use SilverStripe\Forms\CheckboxSetField;

class MaterialOrderPage extends Page
{
    private static $many_many = [
        "Categories" => Category::class,
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Categories', [
            CheckboxSetField::create('Categories', 'visible Products', Category::get()),
        ]);

        return $fields;
    }

    public function getMaterialList()
    {
        return Product::getAllProducts(true)->filter("CategoryID:PartialMatch", $this->Categories()->getIDList());
    }
}
