<?php

use SilverStripe\ORM\DataObject;
use SilverStripe\Security\Permission;
use SilverStripe\Security\PermissionProvider;

class Category extends DataObject implements PermissionProvider
{
    private static $db = [
        "Title" => "Varchar",
    ];

    private static $has_many = [
        "Products" => Product::class,
    ];

    private static $belongs_many_many = [
        "MaterialOrderPages" => MaterialOrderPage::class,
    ];

    const CMS_PERMISSION_CODE = "CMS_ACCESS_" . __CLASS__;

    public function providePermissions()
    {
        $implodedClassName = strtolower(implode(" ", preg_split('/(?=[A-Z])/', __CLASS__)));
        $permissionsArr = [];
        foreach (["view", "edit", "delete", "create"] as $value) {
            $permissionsArr[strtoupper(self::CMS_PERMISSION_CODE . "_" . $value)] = [
                "name" => "Can $value $implodedClassName",
                "category" => "CMS administrate '" . __CLASS__ . "'",
                "help" => "Gives permissions to $value $implodedClassName",
            ];
        }
        return $permissionsArr;
    }

    public function canView($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_VIEW"), "any", $member);
    }

    public function canEdit($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_EDIT"), "any", $member);
    }

    public function canDelete($member = null)
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_DELETE"), "any", $member);
    }

    public function canCreate($member = null, $context = [])
    {
        return Permission::check(strtoupper(self::CMS_PERMISSION_CODE . "_CREATE"), "any", $member);
    }
}
