<?php

use SilverStripe\Admin\ModelAdmin;

class MaterialOrdersAdmin extends ModelAdmin
{
    private static $menu_title = "Material Order";
    private static $url_segment = "materialorder";

    private static $managed_models = [
        "MaterialOrder"
    ];
}
