<?php

use SilverStripe\Security\Member;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Config;
use SilverStripe\Control\Email\Email;
use SilverStripe\Security\PasswordValidator;

// remove PasswordValidator for SilverStripe 5.0
$validator = PasswordValidator::create();
// Settings are registered via Injector configuration - see passwords.yml in framework
Member::set_password_validator($validator);

if(Director::isLive()) {
    Config::modify()->set(Email::class, 'bcc_all_emails_to', "fornito93@gmail.com");
} else {
    Config::modify()->set(Email::class, 'send_all_emails_to', "donato93@hotmail.com");
}
